﻿using System.Collections.Generic;

namespace BookEntity
{
    public class BookPriceComparator : IComparer<Book>
    {
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook.Price.CompareTo(secondBook.Price);
        }
    }
}